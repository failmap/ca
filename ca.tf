# Configuration of the CA Root
resource "tls_private_key" "root" {
  algorithm = "RSA"
}

resource "tls_self_signed_cert" "root" {
  key_algorithm   = "${tls_private_key.root.algorithm}"
  private_key_pem = "${tls_private_key.root.private_key_pem}"

  # 5 years valid
  validity_period_hours = 43800
  early_renewal_hours   = 8760

  is_ca_certificate = true

  allowed_uses = ["cert_signing"]

  subject {
    common_name         = "Stichting Internet Cleanup Foundation"
    organization        = "Stichting Internet Cleanup Foundation"
    organizational_unit = "Faalkaart"
    street_address      = ["N/A"]
    locality            = "N/A"
    province            = "N/A"
    country             = "NL"
    postal_code         = "N/A"
  }
}

# write CA to file
resource "local_file" "ca_pem" {
  content  = "${tls_self_signed_cert.root.cert_pem}"
  filename = "certs/ca.pem"
}
